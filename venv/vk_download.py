import vkstreaming
import websocket
import requests
import json
import csv
import time


from sklearn.metrics import mean_squared_error
import re
from nltk.corpus import stopwords
import pymorphy2
import websocket
import pandas as pd
from google.oauth2 import service_account
from google.cloud import bigquery

project_id = 'arctic-task-238719'
private_key='arctic-task-238719-e6a1c5fe056b.json'
# group_name = ['spb', 'peterburg_dtp','novosti_spb', 'piteronline24','nevablog','pitertv','bspb','topspb_tv','gazetaspbru','zaks']
group_name = ['spb']

def group_info(domain):
    token = 'e7a79876e7a79876e7a79876e9e7ce3561ee7a7e7a79876bb0457d3e507797f75821138'
    version = 5.95

    response= requests.get('https://api.vk.com/method/groups.getById',
                            params={
                                'group_id': domain,
                                'access_token': token,
                                'v': version,
                                'fields':['place']
                            }
                            )

    name_gr=response.json()['response'][0]['name']
    id_gr = response.json()['response'][0]['id']
    gr = pd.DataFrame(columns=['id_group', 'domain', 'name'])

    gr.loc[0] = [id_gr, domain, name_gr]
    gr.to_gbq('my_dataset.group', project_id=project_id, if_exists='append', private_key=private_key)




def post_download(domain):


    token = 'e7a79876e7a79876e7a79876e9e7ce3561ee7a7e7a79876bb0457d3e507797f75821138'
    version = 5.92
    count = 100
    offset = 0
    all_posts = []
    x = 100
    j=0
    while (x==100):
        start_time = time.time()
        response = requests.get('https://api.vk.com/method/wall.get',
                                params={
                                    'access_token': token,
                                    'v': version,
                                    'domain': domain,
                                    'count': count,
                                    'offset':offset
                                }
                                )
        data = response.json()['response']['items']
        offset += 100
        i=0
        j=j+1
        print(j)

        for p in data:
            data_p=p['date']
            post = pd.DataFrame(columns=['id_post', 'domain', 'date', 'text'])

            if ((data_p < 1455621325) and (data_p >= 1451606401)):
                post.loc[0] = [p['id'], domain, p['date'], p['text']]
                post.to_gbq('my_dataset.post2016', project_id=project_id, if_exists='append', private_key=private_key)



        time.sleep(0.5)
        x = len(data)

for gr_name in group_name:
    group_info(gr_name)
    all_posts=post_download(gr_name)


