
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pylab

G = nx.DiGraph()

weight = [0.83, 0.92, 0.58, 0.75, 0.58, 0.75]
G.add_edges_from([('Здравоохранение','Бюджет по расходам')], weight=weight[0])
G.add_edges_from([('Образование','Бюджет по расходам')], weight=weight[1])
G.add_edges_from([('Социальная политика','Бюджет по расходам')], weight=weight[2])
G.add_edges_from([('ЖКХ','Бюджет по расходам')], weight=weight[3])
G.add_edges_from([('Развитие культуры и спорта','Бюджет по расходам')], weight=weight[4])
G.add_edges_from([('Транспорт','Бюджет по расходам')], weight=weight[5])


node_labels={n:"this is node {}".format(n) for n in range(1,5)}

edge_labels=dict([((u,v,),d['weight'])
                 for u,v,d in G.edges(data=True)])
plt.figure()

pos=nx.spring_layout(G)
nx.draw_networkx(G,pos, node_color = 'red', node_size=500,edge_cmap=plt.cm.Reds,with_labels=False)
nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels)
for p in pos:
    t= list(pos[p])
    t[1]=t[1]+0.1
    pos[p]=tuple(t)
nx.draw_networkx_labels(G, pos)

# mng = plt.get_current_fig_manager()
# mng.full_screen_toggle()

plt.savefig("static/Graph.png", format="PNG")
