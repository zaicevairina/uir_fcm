from sklearn.metrics import mean_squared_error
import re
from nltk.corpus import stopwords
import pymorphy2
import requests
import json
import pandas as pd
from google.oauth2 import service_account
import math
project_id = 'arctic-task-238719'
private_key='arctic-task-238719-e6a1c5fe056b.json'
from google.cloud import bigquery
credentials = service_account.Credentials.from_service_account_file('path/to/arctic-task-238719-e6a1c5fe056b.json')
from pandas.io import gbq
stops = set(stopwords.words("english")) | set(stopwords.words("russian"))
morph=pymorphy2.MorphAnalyzer()

dic_zdravoochanenie=['строительство','ремонт','новый','выделить','выделено','субсидия','рубль','оборудование','затрата','финансирование','увеличить']
dic_obrazovanie=['строительство','ремонт','закупка','субсидия','дистанционный','выделено','выделить','рубль','оборудование']
dic_cotial=['субсидии','повышение','капитал','поддержка','выплата','увеличение','увеличить','повысить','затрата','финансирование']
dic_gkch=['подъезд','дом','набережная','мост','парк','двор','площадка']
dic_cultura=['музей','заповедник','особняк','дворец','собор','парк','памятник','наследие']
dic_transport=['дорога','путь','станция','кольцо','шоссе']


def treatment_text(review):
    review_text = re.sub("[^а-яА-Яa-zA-Z0-9]", " ", review)
    words = review_text.lower().split()
    words = [w for w in words if not w in stops]
    words = [morph.parse(w)[0].normal_form for w in words]
    # print(words)
    return(words)

def  dic_zdr(post_tr):
    t=0
    for jj in post_tr:
        for ii in dic_zdravoochanenie:
            if (jj==ii) : t=1
    return(t)

def  dic_obr(post_tr):
    t=0
    for jj in post_tr:
        for ii in dic_obrazovanie:
            if (jj==ii) : t=1
    return(t)

def  dic_cot(post_tr):
    t=0
    for jj in post_tr:
        for ii in dic_cotial:
            if (jj==ii) : t=1
    return(t)

def  dic_g(post_tr):
    t=0
    for jj in post_tr:
        for ii in dic_gkch:
            if (jj==ii) : t=1
    return(t)

def  dic_cul(post_tr):
    t=0
    for jj in post_tr:
        for ii in dic_cultura:
            if (jj==ii) : t=1
    return(t)

def  dic_tr(post_tr):
    t=0
    for jj in post_tr:
        for ii in dic_transport:
            if (jj==ii) : t=1
    return(t)


def concept_zdr():
    f_bol=value_factor(1)
    f_zdr=value_factor(2)
    f_pol=value_factor(3)
    f_rod=value_factor(4)
    f_med=value_factor(5)
    c_zdr=math.pow(f_bol*f_zdr*f_pol*f_rod*f_med,1/5)
    return (c_zdr)

def concept_obr():

    f_det=value_factor(6)
    f_shc=value_factor(7)
    f_obr=value_factor(8)

    c_obr=math.pow(f_det*f_shc*f_obr,1/3)
    return (c_obr)


def concept_cots():

    f_lgot = value_factor(9)
    f_capital = value_factor(10)
    f_coc = value_factor(11)
    if (f_coc==1): c_cots = math.pow(f_lgot * f_coc, 1 / 2)
    else : c_cots = math.pow(f_lgot * f_capital * f_coc, 1 / 3)
    return (c_cots)

def concept_gkh():
    f_gkh = value_factor(12)
    f_remont = value_factor(13)
    f_obust = value_factor(14)

    c_gkh = math.pow(f_gkh * f_remont * f_obust, 1 / 3)
    return (c_gkh)

def concept_cul_sport():
    f_rest = value_factor(15)
    f_remont = value_factor(16)
    f_ochrana = value_factor(17)
    f_sport = value_factor(18)

    c_cul_sport = math.pow(f_rest * f_remont * f_ochrana*f_sport, 1 / 4)
    return (c_cul_sport)

def concept_transport():
    f_remont = value_factor(19)
    f_str = value_factor(20)
    c_tran = math.pow(f_remont * f_str, 1 / 2)
    return (c_tran)

def concept_nalog():
    f_nalog = value_factor(21)

    return (f_nalog)

def value_factor(i):
    f=1
    global count_posts
    k_text_with_i=0
    for gg in range(count_posts):
        if (vec[gg][i] > 0): k_text_with_i += 1
    if (k_text_with_i!=0):
        idf=math.log(count_posts2/k_text_with_i)
    else: return (1)
    for g in range(count_posts):
        if (vec[g][i]>0):
            tf_p=vec[g][i]/vec[g][0]
            tf_idf_p=tf_p*idf
            f=f*tf_idf_p
    f=math.pow(f, 1 / k_text_with_i)
    return(f)

def value_concepts():
    concepts=[]
    concepts.append(0)
    concepts.append(concept_zdr())
    concepts.append(concept_obr())
    concepts.append(concept_cots())
    concepts.append(concept_gkh())
    concepts.append(concept_cul_sport())
    concepts.append(concept_transport())
    # concepts.append(concept_nalog())
    # v = 0.83 * c1 + 0.92 * c2 + 0.58 * c3 + 0.75 * c4 + 0.58 * c5 + 0.75 * c6
    for i in range(7): concepts[i]=round(concepts[i],5)
    return(concepts)

def vector_post(vec,post_tr):
    k = []


    for j in range(21):
        k.append(0)


    for j in post_tr:
        vec[i][0] += 1
        if (j=='больница'):
            t=dic_zdr(post_tr)
            if (t==1):
                vec[i][1]+=1
                k[1]+=1
        if (j == 'здравоохранение'):
            k[2]+=1
            vec[i][2]+=1
        if (j == 'поликлиника'):
            t = dic_zdr(post_tr)
            if (t == 1):
                vec[i][3]+=1
                k[3] += 1
        if (j == 'роддом'):
            t = dic_zdr(post_tr)
            if (t == 1):
                vec[i][4]+=1
                k[4] += 1
        if (j == 'медицина'):
            t = dic_zdr(post_tr)
            if (t == 1):
                vec[i][5]+=1
                k[5] += 1
        if (j == 'сад'):
            t = dic_obr(post_tr)
            if (t == 1):
                vec[i][6]+=1
                k[6] += 1
        if (j == 'школа'):
            t = dic_obr(post_tr)
            if (t == 1):
                vec[i][7]+=1
                k[7] += 1
        if (j == 'образование'):
            t = dic_obr(post_tr)
            if (t == 1):
                vec[i][8]+=1
                k[8] += 1
        if (j == 'льгота'):
            t = dic_cot(post_tr)
            if (t == 1):
                vec[i][9]+=1
                k[9] += 1
        if (j == 'материнский'):
            t = dic_cot(post_tr)
            if (t == 1):
                vec[i][10]+=1
                k[10] += 1
        if (j == 'социальный'):
            t = dic_cot(post_tr)
            if (t == 1):
                vec[i][11]+=1
                k[11] += 1
        if (j == 'жкх'):
            vec[i][12]+=1
            k[12] += 1
        if (j == 'ремонт'):
            t = dic_g(post_tr)
            if (t == 1):
                vec[i][13]+=1
                k[13] += 1
        if (j == 'обустройство'):
            t = dic_g(post_tr)
            if (t == 1):
                vec[i][14]+=1
                k[14] += 1
        if (j == 'реставрация'):
            t = dic_cul(post_tr)
            if (t == 1):
                vec[i][15]+=1
                k[15] += 1
        if (j == 'ремонт'):
            t = dic_cul(post_tr)
            if (t == 1):
                vec[i][16]+=1
                k[16] += 1
        if (j == 'охрана'):
            t = dic_cul(post_tr)
            if (t == 1):
                vec[i][17]+=1
                k[17] += 1
        if (j == 'спорт'):
            vec[i][18]+=1
            k[18] += 1
        if (j == 'ремонт'):
            t = dic_tr(post_tr)
            if (t == 1):
                vec[i][19]+=1
                k[19] += 1
        if (j == 'строительство'):
            t = dic_tr(post_tr)
            if (t == 1):
                vec[i][20]+=1
                k[20] += 1

    return(vec)
import time
start_time = time.time()
# **********
sQuery = '''
    SELECT *
    FROM [my_dataset.post2016]
'''

df = gbq.read_gbq(sQuery, project_id,credentials=credentials)
count_posts=len(df['text'])


df['text'] = df['text'].astype(str)

vec=[]

for i in range(count_posts):
    vec.append([])
    for j in range(22):
        vec[i].append(0)
i=0
count_posts2=0



while (i<count_posts):
    post_tr=treatment_text(df['text'][i])
    vec=vector_post(vec,post_tr)


    i = i + 1

sum_all=0

for h in range(count_posts):
    kk=1
    sum_k=0
    while (kk<22):
        sum_k+=vec[h][kk]
        kk+=1
    if (sum_k>0):
        sum_all=sum_all+vec[h][0]
        count_posts2 += 1

print(2016)
print(value_concepts())
# ********
sQuery = '''
    SELECT *
    FROM [my_dataset.post2017]
'''

df = gbq.read_gbq(sQuery, project_id,credentials=credentials)
count_posts=len(df['text'])


df['text'] = df['text'].astype(str)

vec=[]

for i in range(count_posts):
    vec.append([])
    for j in range(22):
        vec[i].append(0)
i=0
count_posts2=0



while (i<count_posts):
    post_tr=treatment_text(df['text'][i])
    vec=vector_post(vec,post_tr)


    i = i + 1

sum_all=0

for h in range(count_posts):
    kk=1
    sum_k=0
    while (kk<22):
        sum_k+=vec[h][kk]
        kk+=1
    if (sum_k>0):
        sum_all=sum_all+vec[h][0]
        count_posts2 += 1

print(2017)
print(value_concepts())
# ******************
sQuery = '''
    SELECT *
    FROM [my_dataset.post2018]
'''

df = gbq.read_gbq(sQuery, project_id,credentials=credentials)
count_posts=len(df['text'])


df['text'] = df['text'].astype(str)

vec=[]

for i in range(count_posts):
    vec.append([])
    for j in range(22):
        vec[i].append(0)
i=0
count_posts2=0



while (i<count_posts):
    post_tr=treatment_text(df['text'][i])
    vec=vector_post(vec,post_tr)


    i = i + 1

sum_all=0

for h in range(count_posts):
    kk=1
    sum_k=0
    while (kk<22):
        sum_k+=vec[h][kk]
        kk+=1
    if (sum_k>0):
        sum_all=sum_all+vec[h][0]
        count_posts2 += 1

print(2018)
print(value_concepts())
#
print("--- %s seconds ---" % (time.time() - start_time))
# def prognoz(c1,c2,c3,c4,c5,c6):
#
#
#     print(v)
#     return v
#
#
# concepts2016=[0, 0,11867, 0.09611, 0.07943, 0.05141, 0.04978, 0.04758]
# concepts2017=[0, 0.08705, 0.05822, 0.14427, 0.06826, 0.03612, 0.05012]
# concepts2018=[0, 0.02833, 0.02369, 0.04006, 0.0373, 0.02394, 0.02678]
# print(concepts2016[0])
# print(concepts2017[0])
# print(concepts2018[0])
#
# concepts2016[0]=prognoz(concepts2016[1],concepts2016[2],concepts2016[3],concepts2016[4],concepts2016[5],concepts2016[6])
# concepts2017[0]=prognoz(concepts2017[1],concepts2017[2],concepts2017[3],concepts2017[4],concepts2017[5],concepts2017[6])
# concepts2018[0]=prognoz(concepts2018[1],concepts2018[2],concepts2018[3],concepts2018[4],concepts2018[5],concepts2018[6])
# print(concepts2016[0])
# print(concepts2017[0])
# print(concepts2018[0])