from flask import Flask
from flask import render_template
from flask import request
from perevod import *
import math

def prognoz_chod(c1,c2,c3,c4,c5,c6,concepts2018_t,concetpts2018,eps):
    c1=func1(concepts2018_t[1]+c1)
    c2 = func2(concepts2018_t[2]+c2)
    c3 = func3(concepts2018_t[3]+c3)
    c4 = func4(concepts2018_t[4]+c4)
    c5 = func5(concepts2018_t[5]+c5)
    c6 = func6(concepts2018_t[6]+c6)
    v =0.83 * c1 + 0.92 * c2 + 0.58 * c3 + 0.75 * c4 + 0.58 * c5 + 0.95 * c6
    k=0
    budget=concepts2018[0]
    yet=budget
    raz=1-budget
    cn=[0,round(c1,3),round(c2,3),round(c3,3),round(c4,3),round(c5,3),round(c6,3)]
    while (abs(raz)>=eps):
        budget=1/(1+math.exp(-budget-v))
        raz=yet-budget
        yet=budget
        k+=1
    return (k,budget,cn)


def prognoz(c1,c2,c3,c4,c5,c6,concepts2018_t,concetpts2018,k):
    c1=func1(concepts2018_t[1]+c1)
    c2 = func2(concepts2018_t[2]+c2)
    c3 = func3(concepts2018_t[3]+c3)
    c4 = func4(concepts2018_t[4]+c4)
    c5 = func5(concepts2018_t[5]+c5)
    c6 = func6(concepts2018_t[6]+c6)
    cn=[0,round(c1,3),round(c2,3),round(c3,3),round(c4,3),round(c5,3),round(c6,3)]
    v =0.83 * c1 + 0.92 * c2 + 0.58 * c3 + 0.75 * c4 + 0.58 * c5 + 0.95 * c6
    i=1
    budget=concepts2018[0]
    while (i<=k):
        budget=1/(1+math.exp(-budget-v))
        i+=1
    return (budget,cn)




concepts2016_t=[510533687,78248870, 119342693, 64021493,49452675, 38811599, 105882997]
concepts2017_t=[574977091, 93469349, 138876382, 73000698, 58444506, 33926557, 120156040]
concepts2018_t=[588942690,77080555 ,147315298, 99083537, 60295272, 35553539, 108678608]

concepts2016=[0.45, 0.11867, 0.09611, 0.07943, 0.05141, 0.04978, 0.04758]
concepts2017=[0.55, 0.08705, 0.05822, 0.14427, 0.06826, 0.03612, 0.05012]
concepts2018=[0.6, 0.02833, 0.02369, 0.04006, 0.0373, 0.02394, 0.02678]

# print(prognoz(0,0,0,0,0,0,concepts2016_t))

# print(prognoz_chod(3800000,5300000,0,3000000,2800000,2500000,concepts2018_t,concepts2018,0.05))
# print(prognoz(3800000,5300000,0,3000000,2800000,2500000,concepts2018_t,concepts2018,1))



app = Flask(__name__)



@app.route('/',methods=['GET','POST'])
def start():

    if request.method=='POST':
        years=request.form['years']
        c1 = request.form['c1']
        c2 = request.form['c2']
        c3 = request.form['c3']
        c4 = request.form['c4']
        c5 = request.form['c5']
        c6 = request.form['c6']
        eps = request.form['eps']
        count = request.form['count']

        if (request.form['btn']=="Посмотреть"):
            print(years)
            if (years == "2016"):
                return render_template('prediction2016.html', concepts2016=concepts2016,concepts2016_t=concepts2016_t)
            if (years == "2017"):
                return render_template('prediction2017.html', concepts2017=concepts2017,concepts2017_t=concepts2017_t)
            if (years == "2018"):
                return render_template('prediction2018.html', concepts2018=concepts2018,concepts2018_t=concepts2018_t)

        if  (request.form['btn']=="Посчитать"):
            c1 = int(c1)
            c2 = int(c2)
            c3 = int(c3)
            c4 = int(c4)
            c5 = int(c5)
            c6 = int(c6)
            eps =float(eps)
            count = int(count)


            if (eps!=0 and count==0):
                if((c1 != 0) or (c2 != 0) or (c3 != 0) or (c4 != 0) or (c5 != 0) or (c6 != 0)):
                    k, budget,cn = prognoz_chod(c1, c2, c3, c4, c5, c6, concepts2018_t, concepts2018, eps)
                    # print(prognoz_chod(c1, c2, c3, c4, c5, c6, concepts2018_t, concepts2018, eps))
                    budget = round(budget, 5)
                    budget_t = int(round(perevodbudget(budget),0))
                    return render_template('prediction2019.html', c1=c1, c2=c2, c3=c3, c4=c4, c5=c5, c6=c6,
                                           budget=budget, budget_t=budget_t, k=k,concepts2018_t=concepts2018_t,cn=cn)
            if (count!=0 and eps==0):
                if((c1 != 0) or (c2 != 0) or (c3 != 0) or (c4 != 0) or (c5 != 0) or (c6 != 0)):
                    budget,cn = prognoz(c1, c2, c3, c4, c5, c6, concepts2018_t, concepts2018, count)
                    budget = round(budget, 5)
                    budget_t = int(round(perevodbudget(budget),0))
                    return render_template('prediction20k.html', c1=c1, c2=c2, c3=c3, c4=c4, c5=c5, c6=c6,
                                           budget=budget, budget_t=budget_t, count=count,concepts2018_t=concepts2018_t,cn=cn)

            # if ((c1!=0) or (c2!=0) or(c3!=0) or(c4!=0) or(c5!=0) or (c6!=0)):
            #     if (eps!=0):
            #         k,budget = prognoz_chod(c1, c2, c3, c4, c5, c6, concepts2018_t, concepts2018, eps)
            #         print(prognoz_chod(c1, c2, c3, c4, c5, c6, concepts2018_t, concepts2018, eps))
            #         # budget_t=budget_f(budget)
            #         budget = round(budget, 5)
            #         budget_t = 'f(budget)'
            #         return render_template('prediction2019.html', c1=c1, c2=c2, c3=c3, c4=c4, c5=c5, c6=c6,
            #                                budget=budget, budget_t=budget_t, k=1)
            #     if (count!=0):
            #         budget = prognoz(c1, c2, c3, c4, c5, c6, concepts2018_t, concepts2018, count)
            #
            #         budget = round(budget, 5)
            #         budget_t = 'f(budget)'
            #         return render_template('prediction20k.html', c1=c1, c2=c2, c3=c3, c4=c4, c5=c5, c6=c6,
            #                                budget=budget, budget_t=budget_t, count=count)




    return render_template('index.html')



if __name__=='__main__':
    app.run(debug=True)